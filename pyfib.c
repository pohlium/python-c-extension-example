#define PY_SSIZE_T_CLEAN  // make "s#" use Py_ssize_t rather than int (see https://docs.python.org/3.8/extending/extending.html#extracting-parameters-in-extension-functions)
#include <Python.h>

// recursive c implementation of the fibonacci sequence
unsigned long long c_fib_rec(int n){
    if (n < 2) return n;
    else return c_fib_rec(n-1) + c_fib_rec(n-2);
}

// iterative c implementation of the fibonacci sequence
unsigned long long c_fib_iter(int n){
    unsigned long long pre_last = 0;
    unsigned long long last = 1;
    unsigned long long cur = 0;
    if (n < 2) return n;
    for (int i = 2; i <= n; i++){
        cur = last + pre_last;
        pre_last = last;
        last = cur;
    }
    return cur;
}

// the python wrapper for the c_fib_rec function
static PyObject* py_fib(PyObject* self, PyObject* args){
    int n; // this is where we store the argument of the python func
    if (!PyArg_ParseTuple(args, "i", &n)) return NULL; // the "i" tries to cast the `args` tuple to an integer
    return Py_BuildValue("K", c_fib_rec(n)); // parse the output of the fib_rec func back to a python int
}

// the python wrapper for the c_fib_iter function
static PyObject* py_fib_iter(PyObject* self, PyObject* args){
    int n;
    if (!PyArg_ParseTuple(args, "i", &n)) return NULL;
    return Py_BuildValue("K", c_fib_iter(n)); // see https://docs.python.org/3/c-api/arg.html for the type converters
}

// python wrapper for a version function, but its just a static string so we don't actually wrap
// another function.
static PyObject* version(PyObject* self){
    return Py_BuildValue("s", "Version 1.0"); // the "s" casts the C string to a python string
}

// define all the python methods that should be available in the module
static PyMethodDef methods[] = {
    // func_name_in_python, func, arg_type, doc_string
    {"c_fib_rec", py_fib, METH_VARARGS, "Calculates the fibonacci numbers recursively."},
    {"c_fib_iter", py_fib_iter, METH_VARARGS, "Calculates the fibonacci numbers iteratively."},
    {"version", (PyCFunction)version, METH_NOARGS, "Returns the version of the module."},
    {NULL, NULL, 0, NULL}
};

// module information
static struct PyModuleDef fibpy_module = {
    PyModuleDef_HEAD_INIT,
    "fibpy", // name of our module
    "Fibonacci Module", // description of our module
    -1, // global state, w/e that means...
    methods // the methods we wanna use in our module
};

// initializes the module
PyMODINIT_FUNC PyInit_pyfib(void){
    return PyModule_Create(&fibpy_module);
}
