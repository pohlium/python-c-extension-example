from distutils.core import setup, Extension

module = Extension('pyfib', sources = ['pyfib.c'])

setup(name = 'pyfib',
      version = '1.0',
      description = 'Fibonacci package',
      ext_modules = [module])
